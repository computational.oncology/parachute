# Parachute

R (version 4.0.2) and Jupyter (Python 3.8.5) used to create the scripts 

R packages used:
- plyr (version 1.8.6)
- dplyr (version 1.0.0)
- ggplot2 (version 3.3.2)
- tidyverse (version 1.3.0)
- survival (version 3.1.12)
- survminer (version 0.4.7)
- icd.data (version )

Python packages used:
- pandas (version 1.1.0)


# BasicSummary.R
It loads the data, and looks at the age distribution and diagnoses. It does not do much.

# DataDescription.R
It proceeds to have the number of patients, episodes and prescriptions in each table available. It does not do much.

# Extension_data_exploration.R
It looks deeper into the data with survival curves and focuses on the 9/10 most common cancer types. Most of the tables and plot come from this script.